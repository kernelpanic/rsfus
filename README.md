# rsfus

New workflow: actually make something before creating a Git repo with lofty goals..
Wow what a wonderful idea!!!!!

Anyway I wanted to make Windows XP installation media on a USB device (well, an SD card.. long story and Tumblr post soon). This requires some patching of the bootloader, but all the ready-made tools for this are Windows-only, including Rufus, probably the best one. I don't have a Windows installation funnily enough, so I looked at Rufus's source code and extracted/adapted the functionality I needed in this Rust project.

I'm thinking of making a Rust alternative to Rufus which works in some multiplatform way, or at least enough to support running on POSIX platforms, like Linux.